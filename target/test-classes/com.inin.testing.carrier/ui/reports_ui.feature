Feature: Test the LCR Reports UI

    Scenario: View reports page
        Given I am logged into the admin ui
        When I click the reports button
        Then I am shown the reports page

    Scenario: Enter Report Parameters
        Given I am viewing the reports page
        When I enter the report parameters
        Then the parameters are validated and accepted

    Scenario: Load report
