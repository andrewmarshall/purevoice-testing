Feature: Test the LCR Schemas UI

    Scenario: Create Schema
        Given I have created a carrier
        And I am viewing the add schema page
        When I input the correct information
        And I click save
        Then the schema is added
