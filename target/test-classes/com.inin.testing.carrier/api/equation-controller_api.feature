Feature: Test the equation-controller endpoint

    Scenario: View equations
      Given there is no information in the request body
      When I do a get to the eval endpoint
      Then I am shown a list of expressions

