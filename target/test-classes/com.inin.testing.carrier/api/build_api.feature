Feature: Test the lcr build information endpoint

    Scenario: View LCR service build info
        Given I can access the lcr service
        When I do a get to the build info endpoint
        Then I am shown information about the build

    Scenario: View LCR service build version
        Given I can access the lcr service
        When I do a get to the build version endpoint
        Then I am shown the version of the build