Feature: Test the service-controller endpoint

    Scenario: View a list of services for a carrier
      Given I have created a carrier
      When I do a get to the service-controller endpoint
      Then I am shown a list of services for that carrier

    Scenario: Create a service for a carrier
      Given I have created a carrier
      And I have the information needed to create a service
      When I do a post to the service-controller endpoint
      Then a service is created for that carrier

    Scenario: Create service for a carrier using OPTIONS ?

    Scenario: View a service for a carrier
      Given I have created a carrier
      And I have created a service for that carrier
      When I do a get to the service-controller endpoint
      Then I am able to see information about the service of the carrier

    Scenario: Edit a service for a carrier
      Given I have created a carrier
      And I have created a service for that carier
      And I have the information needed to edit a service
      When I do a put to the service-controller endpoint
      Then the service is edited

    Scenario: Delete a service for a carrier
      Given I have created a carrier
      And I have created a service for that carrier
      When I do a delete to the service-controller endpoint
      Then the service is deleted

    Scenario: Edit a service for a carrier using OPTIONS ?