Feature: Test the lcr carriers endpoint

    Scenario: View a list of carriers
        Given there is no information in the request body
        When I do a get to the carriers endpoint
        Then I am shown a list of carriers

    Scenario: Create a carrier
        Given I have the information needed to create a carrier
        When I do a post to the carriers endpoint
        Then a carrier is created

    Scenario: Edit a carrier
        Given I have created a carrier
        And I have the information needed to edit a carrier
        When I do a put to the carriers endpiont
        Then the carrier is edited

    Scenario: View a carrier
        Given I have created a carrier
        When I do a get to the carriers endpoint
        Then I am able to see information about the carrier

    Scenario: Delete a carrier
        Given I have created a carrier
        When I do a delete to the carriers endpiont
        Then the carrier is deleted