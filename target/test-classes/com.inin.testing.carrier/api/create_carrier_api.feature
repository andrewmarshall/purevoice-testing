Feature: Test the lcr carriers endpoint

  Scenario: Create a carrier
    Given I have the information needed to create a carrier
    When I do a post to the carriers endpoint
    Then a carrier is created