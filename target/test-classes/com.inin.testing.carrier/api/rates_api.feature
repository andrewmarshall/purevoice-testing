Feature: Test the lcr rates endpoint

    Scenario: Show ratesheets for a carrier
      Given I have created a carrier
      When I do a get to the rates endpoint
      Then I am shown a list of rates for that carrier

    Scenario: Create ratesheet for a carrier
      Given I have created a carrier
      And I have the information needed to create a ratesheet
      When I do a post to the rates endpoint
      Then a ratesheet is created for that carrier

    Scenario: Crate ratesheet for a carrier using OPTIONS ?1234