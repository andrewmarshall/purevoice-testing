Feature: Test the LCR Ratesheets UI

    Scenario: Upload Ratesheet
        Given I have the information needed to create a ratesheet
        When I upload the ratesheet
        And select a carrier
        And select an effective date
        And click the upload button
        Then the ratesheet is uploaded