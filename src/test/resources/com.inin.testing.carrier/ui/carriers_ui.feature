Feature: Test the LCR Carriers UI

    Scenario: Create a carrier
        Given I have the information needed to create a carrier
        When I click the add carrier button
        And I enter the carrier information
        Then a carrier is created

    Scenario: Edit a carrier
        Given I have created a carrier
        And I click the edit carrier button
        When I modify the carrier information
        Then the carrier is edited

    Scenario: View a carrier
        Given I am viewing the carriers page
        When I select a carrier
        Then i am able to see information about the carrier

    Scenario: Delete a carrier
        Given I have created a carrier
        When I delete a carrier
        Then the carrier is deleted

    Scenario: Carrier Details Tab (add/edit/view?)
        Given I have created a carrier
        When I open the details tab
        Then I am shown detailed information about the carrier

    Scenario: Carrier Address Tab (add/edit/view?)
        Given I have created a carrier
        When I open the address tab
        Then I am shown the address information of the carrier

    Scenario: Carrier Account Manager Tab (add/edit/view?)
        Given I have created a carrier
        When I open the account manager tab
        Then I am shown the account manager information of the carrier

    Scenario: Carrier Services Tab (add/edit/view?) (new)
        Given I have created a carrier
        When I open the services tab
        Then I am shown the services information of the carrier
