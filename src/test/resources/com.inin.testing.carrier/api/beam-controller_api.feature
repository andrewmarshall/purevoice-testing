Feature: Test the lcr beam-controller endpoint

    Scenario: View beams
      Given I have created a carrier
      And there is a ray associated with the carrier
      When I do a get to the beams endpoint
      Then I am shown the beam associated with the carrier ray

    Scenario: 