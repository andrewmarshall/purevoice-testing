package com.inin.testing;

/**
 * Created by AndrewM on 4/27/2015.
 */

import com.inin.testing.common.carriers.Carrier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.testng.Assert;

import com.inin.testing.api.resources.ApiResource;
import com.inin.testing.api.resources.ResourceVersion;
import com.inin.testing.api.rest.RestResult;
import com.inin.testing.common.api.CommonServiceType;
import com.inin.testing.common.carriers.Car_CommonServiceType;
import com.inin.testing.common.api.configurations.v1.dto.Organization;
import com.inin.testing.common.api.configurations.v1.workflows.OrganizationsAPI;
import com.inin.testing.test.BaseApiTest;

public class ApiTest extends BaseApiTest {

    private ThreadLocal<Carrier> carrierResponse = new ThreadLocal();
    Logger log = LoggerFactory.getLogger(ApiTest.class);



    private RestResult<Carrier> postCarrier (Carrier car) {
        ApiResource<Carrier, Carrier> resource =
                new ApiResource<>(Carrier.class, car, ResourceVersion.v1,
                Car_CommonServiceType.LCR.CARRIERS, null);
        return resource.post();
    }
}
