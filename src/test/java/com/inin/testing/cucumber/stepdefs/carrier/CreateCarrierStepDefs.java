package com.inin.testing.cucumber.stepdefs.carrier;

import java.util.List;

import com.inin.testing.common.carriers.Carrier;
import com.inin.testing.common.carriers.RestCall;
import com.inin.testing.common.carriers.CarrierWorkflows;
import com.inin.testing.cucumber.stepdefs.StepDefSetup;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

public class CreateCarrierStepDefs extends RestCall {

    private final Logger log = LoggerFactory.getLogger(CreateCarrierStepDefs.class);

   // protected ThreadLocal<CarrierWorkflows> something = new ThreadLocal<>();

    CarrierWorkflows test = new CarrierWorkflows("511461db-7b8b-4596-a163-9a04b6cdb0a5");
    Carrier anotherTest;

    @Given("^I have the information needed to create a carrier$")
    public void i_have_carrier_info() {
        anotherTest = test.getNewCarrierRequest();
    }

    @When("^I do a post to the carriers endpoint$")
    public void create_carrier_via_api() {

        //Carrier carrier = something.get().getNewCarrierRequest();

       // Carrier originalCarrier = testCarrierPost(carrier);

       // Carrier anotherTest = test.getNewCarrierRequest();

        Carrier response = testCarrierPost(anotherTest);

        //System.out.print(response);
    }

    @Then("^a carrier is created$")
    public void validate_carrier_creation() {

        System.out.print("Carrier successfully created.");
    }
}
