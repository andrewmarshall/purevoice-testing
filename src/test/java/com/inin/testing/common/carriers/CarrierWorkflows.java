package com.inin.testing.common.carriers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.inin.testing.common.utils.RandomGenerator;
import retrofit.client.Response;

import com.inin.testing.api.endpoint.RestEndpointUtils;
import com.inin.testing.api.endpoint.RestResponse;
import com.inin.testing.common.utils.JsonUtils;
import com.inin.testing.common.utils.TypedPredicate;
import com.inin.testing.common.api.rest.PureCloudEndpointFactory;

public class CarrierWorkflows {

    private final CarrierEndpoints endpoints;

    public CarrierWorkflows(String sessionId) {
        endpoints = CarrierEndpointFactory.getPrivateEndpointAdapter(CarrierEndpoints.class, sessionId);
    }

    public RestResponse<Carrier> createCarrier(final Carrier carrier) {
        return RestEndpointUtils.exceptionCatchingCall(new TypedPredicate<RestResponse<Carrier>>() {
            @Override
            public RestResponse<Carrier> invoke() {
                Response response = endpoints.createCarrier(carrier);
                RestResponse<Carrier> responseToRestResult = RestEndpointUtils.responseToRestResponse(response, new TypeReference<Carrier>() {});
                return responseToRestResult;
            }
        });
    }

    //public RestResponse<Carrier> showCarriers(final Carrier carrier) {

    //}

    public Carrier getNewCarrierRequest() {
        return new Carrier.Builder()
            .name("TestAutomationCarrier_"+ RandomGenerator.getRandomAlphaString(5))
            .streetAddressLine1("AddressLine1")
            .streetAddressLine2("AddressLine2")
            .city("Indianapolis")
            .state("Indiana")
            .zipCode("46278")
            .acctMgrFirstName("Andrew")
            .acctMgrLastName("Marshall")
            .acctMgrPhone("3175551234")
            .acctMgrEmail("andrew.marshall@inin.com")
            .cicCode("1234")
            .ocn("567")
            .tollFreeAcceptance(true)
            .supportPhone("3175551234")
            .emergencyServices(true)
            .build();
    }
}
