package com.inin.testing.common.carriers;

import com.inin.testing.common.api.publicapi.v1.workflows.SessionWorkflows;
import com.inin.testing.common.api.rest.PureCloudEndpointAdapter;
import com.inin.testing.common.data.TestDataProvider;
import java.util.ArrayList;
import java.util.List;
import retrofit.client.Header;

public class CarrierEndpointFactory {
    private CarrierEndpointFactory() {

    }

    public static <T> T getPrivateEndpointAdapter(Class<T> clazz, String baseServiceUrl) {
        PureCloudEndpointAdapter endpoint = new PureCloudEndpointAdapter(baseServiceUrl);
        return endpoint.implementEndpointService(clazz);
    }
}
