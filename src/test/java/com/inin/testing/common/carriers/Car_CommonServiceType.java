package com.inin.testing.common.carriers;

/**
 * Created by AndrewM on 4/28/2015.
 */

import com.inin.testing.api.resources.ServiceType;

public interface Car_CommonServiceType extends ServiceType {

    public static enum LCR implements Car_CommonServiceType {
        CARRIER,
        CARRIERS;

        private final String configFilePath = "com/inin/testing/common/carriers";
        private final String yamlFileName = "Endpoints.yaml";

        private LCR() {
        }

        public String getConfigFilePath() {
            this.getClass();
            return "com/inin/testing/common/carriers";
        }

        public String getConfigFileName() {
            this.getClass();
            return "Endpoints.yaml";
        }
    }
}
