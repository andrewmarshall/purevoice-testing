package com.inin.testing.common.carriers;

import java.util.EnumSet;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.StandardToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.math3.random.RandomData;
import org.apache.commons.math3.random.RandomDataImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.inin.testing.common.helpers.I18NHelper;
import com.inin.testing.common.utils.JsonUtils;
import com.inin.testing.common.utils.RandomGenerator;

public class Carrier {

    private Logger log = LoggerFactory.getLogger(getClass());

    private String id;
    private String name;
    private String streetAddressLine1;
    private String streetAddressLine2;
    private String city;
    private String state;
    private String zipCode;

    private String acctMgrFirstName;
    private String acctMgrLastName;
    private String acctMgrPhone;
    private String acctMgrEmail;

    private String cicCode;
    private String ocn;
    private Boolean tollFreeAcceptance;
    private String supportPhone;
    private Boolean emergencyServices;

    private Carrier() {}
    private Carrier(Carrier c) {
        this.id = c.id;
        this.name = c.name;
        this.streetAddressLine1 = c.streetAddressLine1;
        this.streetAddressLine2 = c.streetAddressLine2;
        this.city = c.city;
        this.state = c.state;
        this.zipCode = c.zipCode;
        this.acctMgrFirstName = c.acctMgrFirstName;
        this.acctMgrLastName = c.acctMgrLastName;
        this.acctMgrPhone = c.acctMgrPhone;
        this.acctMgrEmail = c.acctMgrEmail;
        this.cicCode = c.cicCode;
        this.ocn = c.ocn;
        this.tollFreeAcceptance = c.tollFreeAcceptance;
        this.supportPhone = c.supportPhone;
        this.emergencyServices = c.emergencyServices;
    }

    public String getId() {
        return id;
    }

    public Carrier setId(String id) {
        Carrier ret = new Carrier(this);
        ret.id = id;
        return ret;
    }

    public String getName() {
        return name;
    }

    public Carrier setName(String name) {
        Carrier ret = new Carrier(this);
        ret.name = name;
        return ret;
    }

    public String getAddressLine2() {
        return streetAddressLine2;
    }

    public Carrier setAddressLine2(String addressLine2) {
        Carrier ret = new Carrier(this);
        ret.streetAddressLine2 = streetAddressLine2;
        return ret;
    }

    public String getCity() {
        return city;
    }

    public Carrier setCity(String city) {
        Carrier ret = new Carrier(this);
        ret.city = city;
        return ret;
    }

    public String getState() {
        return state;
    }

    public Carrier setState(String state) {
        Carrier ret = new Carrier(this);
        ret.state = state;
        return ret;
    }

    public String getZipCode() {
        return zipCode;
    }

    public Carrier setZipCode(String zipCode) {
        Carrier ret = new Carrier(this);
        ret.zipCode = zipCode;
        return ret;
    }

    public String getAcctMgrFirstName() {
        return acctMgrFirstName;
    }

    public Carrier setAcctMgrFirstName(String acctMgrFirstName) {
        Carrier ret = new Carrier(this);
        ret.acctMgrFirstName = acctMgrFirstName;
        return ret;
    }

    public String getAcctMgrLastName() {
        return acctMgrLastName;
    }

    public Carrier setAcctMgrLastName(String acctMgrLastName) {
        Carrier ret = new Carrier(this);
        ret.acctMgrLastName = acctMgrLastName;
        return ret;
    }

    public String getAcctMgrPhone() {
        return acctMgrPhone;
    }

    public Carrier setAcctMgrPhone(String acctMgrPhone) {
        Carrier ret = new Carrier(this);
        ret.acctMgrPhone = acctMgrPhone;
        return ret;
    }

    public String getAcctMgrEmail() {
        return acctMgrEmail;
    }

    public Carrier setAcctMgrEmail(String acctMgrEmail) {
        Carrier ret = new Carrier(this);
        ret.acctMgrEmail = acctMgrEmail;
        return ret;
    }

    public String getCicCode() {
        return cicCode;
    }

    public Carrier setCicCode(String cicCode) {
        Carrier ret = new Carrier(this);
        ret.cicCode = cicCode;
        return ret;
    }

    public String getOcn() {
        return ocn;
    }

    public Carrier setOcn(String ocn) {
        Carrier ret = new Carrier(this);
        ret.ocn = ocn;
        return ret;
    }

    public Boolean getTollFreeAcceptance() {
        return tollFreeAcceptance;
    }

    public Carrier setTollFreeAcceptance(Boolean tollFree) {
        Carrier ret = new Carrier(this);
        ret.tollFreeAcceptance = tollFree;
        return ret;
    }

    public String getSupportPhone() {
        return supportPhone;
    }

    public Carrier setSupportPhone(String addressLine1) {
        Carrier ret = new Carrier(this);
        ret.supportPhone = supportPhone;
        return ret;
    }

    public Boolean getEmergencyServices() {
        return emergencyServices;
    }

    public Carrier setEmergencyServices(Boolean emergencyServices) {
        Carrier ret = new Carrier(this);
        ret.emergencyServices = emergencyServices;
        return ret;
    }

    public static class Builder {

        private Carrier carrier;
        private String uniqueName;

        public Builder() {
            carrier = new Carrier();
            uniqueName = null;
        }

        public Builder id(String id) {
            carrier.id = id;
            return this;
        }

        public Builder name(String name) {
            carrier.name = name;
            return this;
        }

        public Builder streetAddressLine1(String streetAddressLine1) {
            carrier.streetAddressLine1 = streetAddressLine1;
            return this;
        }

        public Builder streetAddressLine2(String streetAddressLine2) {
            carrier.streetAddressLine2 = streetAddressLine2;
            return this;
        }

        public Builder city(String city) {
            carrier.city = city;
            return this;
        }

        public Builder state(String state) {
            carrier.state = state;
            return this;
        }

        public Builder zipCode(String zipCode) {
            carrier.zipCode = zipCode;
            return this;
        }

        public Builder acctMgrFirstName(String acctMgrFirstName) {
            carrier.acctMgrFirstName = acctMgrFirstName;
            return this;
        }

        public Builder acctMgrLastName(String acctMgrLastName) {
            carrier.acctMgrLastName = acctMgrLastName;
            return this;
        }

        public Builder acctMgrPhone(String acctMgrPhone) {
            carrier.acctMgrPhone = acctMgrPhone;
            return this;
        }

        public Builder acctMgrEmail(String acctMgrEmail) {
            carrier.acctMgrEmail = acctMgrEmail;
            return this;
        }

        public Builder cicCode(String cicCode) {
            carrier.cicCode = cicCode;
            return this;
        }

        public Builder ocn(String ocn) {
            carrier.ocn = ocn;
            return this;
        }

        public Builder tollFreeAcceptance(Boolean tollFreeAcceptance) {
            carrier.tollFreeAcceptance = tollFreeAcceptance;
            return this;
        }

        public Builder supportPhone(String supportPhone) {
            carrier.supportPhone = supportPhone;
            return this;
        }

        public Builder emergencyServices(Boolean emergencyServices) {
            carrier.emergencyServices = emergencyServices;
            return this;
        }

        public Builder uniqueName(String uniqueName) {
            this.uniqueName = uniqueName;
            return this;
        }

        /**
         * Builds the Carrier
         * Populates all null fields with defaults, then checks that all fields are valid for carrier creation.
         * @return Carrier
         */
        public Carrier build() {
            // Populates all null fields with defaults, then checks that all fields are valid for carrier creation.
            carrier.log.debug("Filling in null fields of carrier with default values");

            // the uniqueName is used to form fields TBD
            if (uniqueName == null) {
                uniqueName = CarrierNameAuthority.CARRIER.generateName();
            }

            if (carrier.name == null) {
                carrier.name = ((uniqueName.length() + 5 > 140) ? uniqueName.subSequence(0,135) : uniqueName) + ", Bob";

                if (carrier.name.length() == 140) {
                    carrier.name = I18NHelper.addNonEnglishCharacter(carrier.name.substring(0,139));
                } else {
                    carrier.name = I18NHelper.addNonEnglishCharacter(carrier.name);
                }
                carrier.log.debug("Setting carrier name {}", carrier.name);
            }

            if (carrier.streetAddressLine1 == null) {
                carrier.streetAddressLine1 = "123 Sesame Street";
                carrier.log.debug("Setting carrier line 1 address to {}", carrier.streetAddressLine1);
            }

            if (carrier.streetAddressLine2 == null) {
                carrier.streetAddressLine2 = "Red Monster Suite";
                carrier.log.debug("Setting carrier line 2 address to {}", carrier.streetAddressLine2);
            }

            if (carrier.city == null) {
                carrier.city = "Indianapolis";
                if (carrier.city.length() < 140) {
                    carrier.city = I18NHelper.addNonEnglishCharacter(carrier.city);
                }
                carrier.log.debug("Setting carrier city to {}", carrier.city);
            }

            if (carrier.state == null) {
                carrier.state = "Indiana";
                carrier.log.debug("Setting carrier state to {}", carrier.state);
            }

            if (carrier.zipCode == null) {
                carrier.zipCode = "46278";
                carrier.log.debug("Setting carrier zip code to {}", carrier.zipCode);
            }

            if (carrier.acctMgrFirstName == null) {
                carrier.acctMgrFirstName = "John";
                carrier.log.debug("Setting carrier account manager first name to {}", carrier.acctMgrFirstName);
            }

            if (carrier.acctMgrLastName == null) {
                carrier.acctMgrLastName = "Smith";
                carrier.log.debug("Setting carrier account manager last name to {}", carrier.acctMgrLastName);
            }

            if (carrier.acctMgrPhone == null) {
                RandomData random = new RandomDataImpl();

                int areaCode = 317;
                // importing list for local classification numbers; same as TestUser
                int prefix = RandomGenerator.getRandomItem(Lists.newArrayList(715, 881, 817, 363));
                int line = random.nextInt(1, 9999);
                String phoneNumber = String.format("%03d%03d%04d", areaCode, prefix, line);
                carrier.acctMgrPhone = phoneNumber;

                carrier.log.debug("Setting carrier account manager phone to {}", carrier.acctMgrPhone);
            }

            if (carrier.acctMgrEmail == null) {
                carrier.acctMgrEmail = carrier.acctMgrFirstName + "." + carrier.acctMgrLastName + "@example.com";
                carrier.log.debug("Setting carrier account manager email to {}", carrier.acctMgrEmail);
            }

            if (carrier.cicCode == null) {
                RandomData random = new RandomDataImpl();
                int code = random.nextInt(1, 9999);
                //ensures code meets the required 4 digits
                String strCode = String.format("%04d", code);
                carrier.cicCode = strCode;

                carrier.log.debug("Setting carrier cic code to {}", carrier.cicCode);
            }

            if (carrier.ocn == null) {
                RandomData random = new RandomDataImpl();
                // uses 4 character code consisting of 3 numeric characters and 1 alpha character
                int partialValue = random.nextInt(1, 999);
                String strPartial = String.format("%03d", partialValue);
                carrier.ocn = strPartial + RandomGenerator.getRandomUpperCaseAlphaString(1);

                carrier.log.debug("Setting carrier ocn to {}", carrier.ocn);
            }

            if (carrier.tollFreeAcceptance == null) {
                carrier.tollFreeAcceptance = true;
                carrier.log.debug("Setting carrier toll free acceptance to {}", carrier.tollFreeAcceptance);
            }

            if (carrier.supportPhone == null) {
                RandomData random = new RandomDataImpl();

                int areaCode = 317;
                //importing list for local classification numbers; same as TestUser
                int prefix = RandomGenerator.getRandomItem(Lists.newArrayList(715, 881, 817, 363));
                int line = random.nextInt(1, 9999);
                String phoneNumber = String.format("%03d%03d%04d", areaCode, prefix, line);
                carrier.supportPhone = phoneNumber;

                carrier.log.debug("Setting carrier support phone to {}", carrier.supportPhone);
            }

            if (carrier.emergencyServices == null) {
                carrier.emergencyServices = true;
                carrier.log.debug("Setting carrier emergency services to {}", carrier.emergencyServices);
            }

            return carrier;
        }
    }

    @Override
    public String toString() {
        return JsonUtils.getPrettyPrintedJson(this);
    }

    public String toInfoLog() {
        StandardToStringStyle style = new StandardToStringStyle();
        style.setContentStart("\n\t");
        style.setFieldNameValueSeparator(": ");
        style.setFieldSeparator(",\n\t");
        style.setUseIdentityHashCode(false);
        return new ToStringBuilder(this, style)
                .append("PureCloud Id", id)
                .append("Name", name)
                .append("Street Address Line 1", streetAddressLine1)
                .append("Street Address Line 2", streetAddressLine2)
                .append("City", city)
                .append("State", state)
                .append("Zip Code", zipCode)
                .append("Account Manager First Name", acctMgrFirstName)
                .append("Account Manager Last Name", acctMgrLastName)
                .append("Account Manager Phone", acctMgrPhone)
                .append("Account Manager Email", acctMgrEmail)
                .append("CIC Code", cicCode)
                .append("OCN", ocn)
                .append("Toll Free Acceptance", tollFreeAcceptance)
                .append("Support Phone", supportPhone)
                .append("Emergency Services", emergencyServices)
                .build();
    }

    @Override
    public int hashCode() {
        return super.hashCode() + new HashCodeBuilder()
                .append(this.id)
                .append(this.name)
                .append(this.streetAddressLine1)
                .append(this.streetAddressLine2)
                .append(this.city)
                .append(this.state)
                .append(this.zipCode)
                .append(this.acctMgrFirstName)
                .append(this.acctMgrLastName)
                .append(this.acctMgrPhone)
                .append(this.acctMgrEmail)
                .append(this.cicCode)
                .append(this.ocn)
                .append(this.tollFreeAcceptance)
                .append(this.supportPhone)
                .append(this.emergencyServices)
                .hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != getClass()) {
            return false;
        }
        if (o == this) {
            return true;
        }
        Carrier other = (Carrier) o;
        return new EqualsBuilder()
                .append(this.id, other.id)
                .append(this.name, other.name)
                .append(this.streetAddressLine1, other.streetAddressLine1)
                .append(this.streetAddressLine2, other.streetAddressLine2)
                .append(this.city, other.city)
                .append(this.state, other.state)
                .append(this.zipCode, other.zipCode)
                .append(this.acctMgrFirstName, other.acctMgrFirstName)
                .append(this.acctMgrLastName, other.acctMgrLastName)
                .append(this.acctMgrPhone, other.acctMgrPhone)
                .append(this.acctMgrEmail, other.acctMgrEmail)
                .append(this.cicCode, other.cicCode)
                .append(this.ocn, other.ocn)
                .append(this.tollFreeAcceptance, other.tollFreeAcceptance)
                .append(this.supportPhone, other.supportPhone)
                .append(this.emergencyServices, other.emergencyServices)
                .isEquals();
    }
}
