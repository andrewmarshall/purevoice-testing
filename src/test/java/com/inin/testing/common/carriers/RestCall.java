package com.inin.testing.common.carriers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.inin.testing.api.rest.RestEndpointClient;
import com.inin.testing.api.endpoint.RestResponse;
import com.inin.testing.api.endpoint.RestEndpointUtils;
import retrofit.client.Response;

import com.inin.testing.common.utils.TypedPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.testng.Assert;

public class RestCall {

    //public ThreadLocal<CarrierWorkflows> something = new ThreadLocal<>();

    CarrierWorkflows test = new CarrierWorkflows("http://internal-lcr-elb-1772485328.us-east-1.elb.amazonaws.com");

    protected Carrier testCarrierPost (Carrier infoToPost)  {
        //RestResponse<Carrier> response = something.get().createCarrier(infoToPost);
        //Assert.assertEquals(response.getStatus(), 200, "Did not receive 200 on the create request.");

        RestResponse<Carrier> secondResponse = test.createCarrier(infoToPost);
        Assert.assertEquals(secondResponse.getStatus(), 201, "Did not receive 201 on the create request.");

        //String newString = secondResponse.getBody().toString();

        return secondResponse.getBody();
    }

}
