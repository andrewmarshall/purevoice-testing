package com.inin.testing.common.carriers;

import com.google.common.base.Preconditions;
import com.inin.testing.common.utils.RandomGenerator;
import java.util.regex.Pattern;

public enum CarrierNameAuthority {
    CARRIER("AutoCarrier-[a-z]{5}.*") {
        public String generateName() {
            return "AutoCarrier_" + RandomGenerator.getRandomAlphaString(5) + "end";
        }
    };

    private Pattern pattern;

    private CarrierNameAuthority(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    private static String generateNameWithThread(String prependStatic, String prependRandom) {
        return prependStatic + prependRandom + Thread.currentThread().getName();
    }

    public boolean matchesPattern(String testName) {
        return this.pattern.matcher(testName).matches();
    }

    public boolean safeToDelete(String name) {
        return this.pattern.matcher(name).matches() && !name.toLowerCase().contains("donotdelete");
    }

    private static String truncate(String name, int minLength, int maxLength) {
        Preconditions.checkArgument(minLength <= maxLength, "Min length cannot be greater than max length");
        if(name.length() < minLength) {
            name = name + RandomGenerator.getRandomAlphaString(minLength - name.length());
        } else if(name.length() > maxLength) {
            name = name.substring(0, maxLength);
        }

        return name;
    }

    public String generateName(int minLength, int maxLength) {
        return truncate(this.generateName(), minLength ,maxLength);
    }

    public abstract String generateName();
}
