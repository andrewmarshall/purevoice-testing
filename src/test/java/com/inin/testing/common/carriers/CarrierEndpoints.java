package com.inin.testing.common.carriers;

import java.util.Map;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.DELETE;
import retrofit.http.PUT;
import com.inin.testing.api.endpoint.RestResponse;

import com.inin.testing.api.resources.ResourceVersion;

public interface CarrierEndpoints {

    @GET("/lcr/carriers")
    Response getCarriers();

    @POST("/lcr/carriers")
    <T> Response createCarrier(@Body T carrier);

    @GET("/lcr/carriers/{carrierId}")
    Response getCarrier(@Path("carrierId") String carrierId);

    @PUT("/lcr/carriers/{carrierId}")
    Response updateCarrier(@Path("carrierId") String carrierId);

    @DELETE("/lcr/carriers/{carrierId}")
    Response deleteCarrier(@Path("carrierId") String carrierId);
}
